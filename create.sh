yc iam service-account create --name kursach-k8s-sa

yc resource-manager folder add-access-binding  default --role=editor --service-account-name=kursach-k8s-sa
yc resource-manager folder add-access-binding  default --role=container-registry.images.puller    --service-account-name=kursach-k8s-sa
yc resource-manager folder add-access-binding  default --role=viewer --service-account-name=kursach-k8s-sa

#yc iam service-account set-access-bindings kursach-k8s-sa \
#  --access-binding role=editor,subject=userAccount:gfei8n54hmfhuk5nogse \
#  --access-binding role=viewer,subject=userAccount:helj89sfj80aj24nugsz

yc managed-kubernetes cluster create \
  --name kursach-k8s \
  --network-name default \
  --zone ru-central1-a \
  --subnet-name default-ru-central1-a \
  --public-ip \
  --release-channel regular \
  --version 1.22 \
  --cluster-ipv4-range 10.1.0.0/16 \
  --service-ipv4-range 10.2.0.0/16 \
  --service-account-name kursach-k8s-sa \
  --node-service-account-name kursach-k8s-sa

yc managed-kubernetes node-group create \
  --cluster-name kursach-k8s \
  --cores 4 \
  --core-fraction 20 \
  --disk-size 30 \
  --disk-type network-hdd \
  --fixed-size 3 \
  --memory 8 \
  --name k8s-nodes \
  --platform-id standard-v3 \
  --network-interface subnets=default-ru-central1-a,ipv4-address=nat \
  --container-runtime containerd \
  --preemptible \
  --version 1.22

yc managed-kubernetes cluster get-credentials kursach-k8s --external --force
