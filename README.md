# Курсовой проект по Kubernetes

## Состав приложений развернутых в кластере

Infra:
- Loki
- Prometheus
- Grafana
- ArgoCD
- Nexus

Hipster shop:
 - cartservice
 - checkoutservice
 - currencyservice
 - frontend
 - paymentservice
 - productcatalogservice
 - redis
 - shippingservice

## Описание процесса поставки приложений.
 - Приложения собираются по правилам описанным в .gitlab-ci.yml в каждом репозитории группы https://gitlab.com/otus-k8s-kursach/hipster-shop
 - Готовые образы контейнеров приложений публикуются в Regestry, который в свою очередь запущен средствами Nexus на том-же кластере https://registry.yc.ga66a.ru/
 - Сервисы приложения Hispter Shop поднимаются и обновляются на кластере при помощи ArgoCD по манифестам находящимися по пути https://gitlab.com/otus-k8s-kursach/infra/-/tree/main/hipster-shop 
 - Приложение доступно по адресу: https://shop.yc.ga66a.ru/